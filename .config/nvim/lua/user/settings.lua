vim.opt.number = true

vim.opt.clipboard:append('unnamedplus')

vim.opt.spelllang = 'en_us'
vim.opt.spell = true

vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2

vim.opt.undofile = true

vim.opt.linespace = 0
vim.opt.scrolloff = 10
vim.opt.mouse = 'a'
vim.opt.mousemoveevent = true
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.autoread = true
vim.opt.list.listchars = 'tab:»·,trail:·'
vim.opt.backspace = 'indent,eol,start'

vim.opt.hlsearch = false

vim.opt.smartindent = true
vim.opt.autoindent = true
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.expandtab = false

vim.opt.whichwrap = '<,>,h,l,[,]'

vim.opt.number = true
vim.opt.numberwidth = 3

vim.opt.foldenable = true
vim.opt.foldopen = 'block,hor,mark,percent,quickfix,tag'
vim.opt.foldmethod = 'indent'
vim.opt.foldlevel = 999
vim.opt.fillchars = 'fold:\\'

vim.opt.laststatus = 2
vim.opt.showtabline = 2

vim.opt.termguicolors = true
