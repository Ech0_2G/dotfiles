local Plugin = {"kylechui/nvim-surround"}

Plugin.version = "*"
Plugin.event = "VeryLazy"
Plugin.config = function()
	require("nvim-surround").setup({})
end

return Plugin
