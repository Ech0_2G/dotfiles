local Plugin = { "nvim-treesitter/nvim-treesitter-textobjects" }

Plugin.dependencies = { "nvim-treesitter/nvim-treesitter" }

Plugin.config = function()
	require("nvim-treesitter.configs").setup({
		textobjects = {
			select = {
				enable = true,
				keymaps = {
					["ab"] = "@block.outer",
					["ib"] = "@block.inner",
					["af"] = "@function.outer",
					["if"] = "@function.inner",
					["am"] = "@class.outer",
					["im"] = "@class.inner",
					["ap"] = "@parameter.outer",
					["ip"] = "@parameter.inner",
				},
			},
		},
	})
end

return Plugin
