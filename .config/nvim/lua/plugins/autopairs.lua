local Plugin = {'windwp/nvim-autopairs'}

Plugin.event = "InsertEnter"
Plugin.opts = {} -- this is equalent to setup({}) function

return Plugin
