local Plugin = {"rcarriga/nvim-notify"}

Plugin.config = function()
	require("notify").setup({
		background_colour = '#000000'
	})
end

return Plugin
